<?php
/**
 * @file
 * Function to delete an event on OpenAgenda with API
 */


/**
 * Implements hook_openagenda_connector_delete()
 * @param string $token
 * @param int $eventUid
 * @see "Deleting an event" part in https://openagenda.zendesk.com/hc/fr/articles/203155742-Publish-content-through-the-API
 */
function _openagenda_connector_delete($token, $eventUid) {

  $url_connect = variable_get_value('open_agenda_end_point') . '/events/' . $eventUid;
  $ch = curl_init($url_connect);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
      'access_token' => $token,
      'nonce' => rand(),
  ));

  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

  $received_content = curl_exec($ch);

  if (curl_getinfo($ch, CURLINFO_HTTP_CODE)==200) {
    //deletion of informations in the dedicated table openagenda_connector
    $num = db_delete('openagenda_connector')
        ->condition('uid', $eventUid)
        ->execute();

    drupal_set_message('The event has been deleted', 'status');

    //log mode
    openagenda_connector_log_mode('Call to openagenda_connector_delete function : Deletion of event n°' . $eventUid, WATCHDOG_NOTICE);
  }
}
