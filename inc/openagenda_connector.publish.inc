<?php
/**
 * @file
 * Function to create or update an event on OpenAgenda with API
 */

/**
 * Implements hook_openagenda_connector_create()
 * @param string $token
 * @param array $data structured data
 * @see "Event data structure" part in https://openagenda.zendesk.com/hc/fr/articles/203155742-Publish-content-through-the-API
 * @return int $event_uid
 */
function _openagenda_connector_create($token, $data) {

  $url_connect = variable_get_value('open_agenda_end_point') . '/events';

  $ch = curl_init($url_connect);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, true);

  $eventData = $data['data'];

  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
    'access_token' => $token,
    'nonce' => rand(),
    'lang' => $data['language'],
    'image' => $data['imagePath'],
    'publish' => false,
    'data' => json_encode($eventData)
  ));

  $received_content = curl_exec($ch);

  $response = json_decode($received_content, true);

  if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
    openagenda_connector_log_mode('Call to openagenda_connector_create function : Could not create the event '. $response['error_description'] . ' Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE), WATCHDOG_NOTICE);
    throw new Exception('Could not create the event' . ($response !== null?': ' . $response['error_description']:'') . ' Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE));
  }
  $event_uid = $response['uid'];

  //storage of return informations in the dedicated table openagenda_connector
  $num = db_insert('openagenda_connector')
    ->fields(array(
      'nid' => $data['nid'],
      'uid' => $response['uid'],
      'changed' => REQUEST_TIME,
      'data' => serialize($response)
    ))->execute();

  //At creation, association to the agenda
  if ($data['agenda_uid']) {
    openagenda_connector_associate_event_to_agenda($data['agenda_uid'], $event_uid, $token, $data['category']);
    openagenda_connector_log_mode('Call to openagenda_connector_create function : assocation between event ' . $event_uid . ' and agenda ' . $data['agenda_uid'], WATCHDOG_NOTICE);
  } else {
    openagenda_connector_log_mode('Call to openagenda_connector_create function : Could not associate event to agenda because no agenda UID is found', WATCHDOG_NOTICE);
    throw new Exception('Could not associate event to agenda because no agenda UID is found');
  }

  //debug mode
  openagenda_connector_debug_mode('openagenda_connector_create', $response);
  //log mode
  openagenda_connector_log_mode('Call to openagenda_connector_create function', WATCHDOG_NOTICE);

  return $event_uid;
}


/**
 * Implements hook_openagenda_connector_update()
 * @param string $token
 * @param int $eventUid
 * @param array $data strutured data
 * @see "Event data structure" part in https://openagenda.zendesk.com/hc/fr/articles/203155742-Publish-content-through-the-API
 */
function _openagenda_connector_update($token, $eventUid, $data) {

  $url_connect = variable_get_value('open_agenda_end_point') . '/events/' . $eventUid;
  $ch = curl_init($url_connect);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $eventData = $data['data'];

  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
    'access_token' => $token,
    'nonce' => rand(),
    'lang' => $data['language'],
    'image' => $data['imagePath'],
    'data' => json_encode($eventData)
  ));

  $received_content = curl_exec($ch);
  $response = json_decode($received_content, true);

  if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
    openagenda_connector_log_mode('Call to openagenda_connector_update function : Could not update the event ' . $response['error_description'] . ' Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE), WATCHDOG_NOTICE);
    throw new Exception('Could not update the event' . ($response !== null?': ' . $response['error_description']:'') . ' Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE));
  }
  $event_uid = $response['uid'];

  //storage of return informations in the dedicated table openagenda_connector
  $num = db_update('openagenda_connector')
      ->fields(array(
        'nid' => $data['nid'],
        'changed' => REQUEST_TIME,
        'data' => serialize($response)
      ))
      ->condition('uid', $event_uid, '=')
      ->execute();

  //debug mode
  openagenda_connector_debug_mode('openagenda_connector_update', $response);
  //log mode
  openagenda_connector_log_mode('Call to openagenda_connector_update function', WATCHDOG_NOTICE);
}



/*
 * Associate an event to an agenda
 * @param int $agendaUid
 * @param int $eventUid
 * @param string t$oken
 * @param string $category
 */
function _openagenda_connector_associate_event_to_agenda($agendaUid, $eventUid, $token, $category) {

  if ($agendaUid && $eventUid) {

    $url_connect = variable_get_value('open_agenda_end_point');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, "$url_connect/agendas/$agendaUid/events");

    $shareData = array(
      'event_uid' => $eventUid,
      'category' => $category
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
      'access_token' => $token,
      'nonce' => rand(),
      'data' => json_encode($shareData),
    ));

    $received_content = curl_exec($ch);

    $response = json_decode($received_content, true);

    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
      openagenda_connector_log_mode('Call to openagenda_connector_associate_event_to_agenda function : Could not add the event to the agenda '.$response['error_description'] . ' Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE), WATCHDOG_NOTICE);
      throw new Exception('Could not add the event to the agenda' . ($response !== null?': ' . $response['error_description']:'') . ' Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE));
    }

    //debug mode
    openagenda_connector_debug_mode('openagenda_connector_associate_event_to_agenda', $response);
    //log mode
    openagenda_connector_log_mode('Call to openagenda_connector_associate_event_to_agenda function', WATCHDOG_NOTICE);
  }
}

