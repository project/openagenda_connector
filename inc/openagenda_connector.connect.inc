<?php
/**
 * @file
 * Function to do the connection to OpenAgenda with API
 */


/**
 * Fonction to connect to the API
 * param string $api_key
 * return string $token
 * @see "Publishing on OpenAgenda - Authentication" part in https://openagenda.zendesk.com/hc/fr/articles/203155742-Publish-content-through-the-API
 */
function _openagenda_connector_connect($api_key) {
  $token = '';

  if (!$api_key) {
    $api_key = variable_get_value('account_secret_api_key');
    if (!$api_key) {
      drupal_set_message('Impossible to connect to Open Agenda API. Check your SECRET API KEY.', 'error');
      openagenda_connector_log_mode('Call to openagenda_connector_connect function : No API KEY found', WATCHDOG_ERROR);
      return false;
    }
  }
  $url_connect = variable_get_value('open_agenda_end_point') . '/requestAccessToken';

  $ch = curl_init($url_connect);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
      'grant_type' => 'authorization_code',
      'code' => $api_key
  ));

  $received_content = curl_exec($ch);
  $response = json_decode($received_content, true);

  if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
    openagenda_connector_log_mode('Call to openagenda_connector_connect function : Could not retrieve token - Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE), WATCHDOG_ERROR);
    throw new Exception('Could not retrieve token - Error code : ' . curl_getinfo($ch, CURLINFO_HTTP_CODE));
  }

  $token = $response['access_token'];

  //debug mode
  openagenda_connector_debug_mode('openagenda_connector_connect', $response);
  //log mode
  openagenda_connector_log_mode('Call to openagenda_connector_connect function : token ('.$token.')', WATCHDOG_NOTICE);

  return $token;
}
