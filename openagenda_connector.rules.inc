<?php

/**
 * Implement hook_rules_action_info().
 */
function openagenda_connector_rules_action_info() {
  return array(
      'openagenda_connector_rules_action_delete_on_openagenda' => array(
          'label' => t('Delete on OpenAgenda'),
          'group' => t('Custom'),
          'parameter' => array(
              'node' => array(
                  'type' => 'node',
                  'label' => t('Node to be deleted in OpenAgenda'),
              ),
          ),
      ),
      'openagenda_connector_rules_action_publish_on_openagenda' => array(
          'label' => t('Publish on OpenAgenda'),
          'group' => t('Custom'),
          'parameter' => array(
              'node' => array(
                  'type' => 'node',
                  'label' => t('Node to be inserted or updated in OpenAgenda'),
              ),
          ),
      ),
  );
}

// This callback automatically delete the event in OpenAgenda
function openagenda_connector_rules_action_delete_on_openagenda($node) {

  //check if the node exist into openagenda_connector table
  $result = db_select('openagenda_connector', 'oc')
      ->fields('oc')
      ->condition('nid', $node->nid, '=')
      ->execute()
      ->fetchAssoc();

  if (!empty($result) && $result['uid'] != 0) {
    $token = openagenda_connector_connect();
    openagenda_connector_delete($token, $result['uid']);
  }
}

// This callback automatically insert or update the event in OpenAgenda
function openagenda_connector_rules_action_publish_on_openagenda($node) {


  //check if the node exist into openagenda_connector table
  $result = db_select('openagenda_connector', 'oc')
      ->fields('oc')
      ->condition('nid', $node->nid, '=')
      ->execute()
      ->fetchAssoc();

  $token = openagenda_connector_connect();

  if (!empty($result) && $result['uid'] != 0) {
    openagenda_connector_update($token, $result['uid'], $node);
  } else {
    openagenda_connector_create($token, $node);
  }
}
