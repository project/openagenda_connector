CONTENTS OF THIS FILE
---------------------

 * Introduction
 * About openagenda.com
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * API Documentation


INTRODUCTION
------------

This module allow you to automatically publish, update or delete content on OpenAgenda (https://openagenda.com/)

ABOUT OPENAGENDA
------------

OpenAgenda let you reference and broadcast all your events.

REQUIREMENTS
------------

This module requires the following modules:
 * Rules (https://www.drupal.org/project/rules)

This module requires no additional library.

You must create an account on https://openagenda.com.

Then you must obtain a secret key. Generating your own secret key from your OpenAgenda account is a feature that is activated
by sending a request at the following email: gimmeanapikey@openagenda.com


INSTALLATION
------------

 * Install and enable this module like any other drupal 7 module.


CONFIGURATION
-------------

 * Enable the openagenda connector module on your main site.
 * Go to the configuration page (admin/config/services/openagenda-connector) and fill
   your settings :
    - Base URL of the API (https://api.openagenda.com/v1)
    - Base URL for public access (http://openagenda.com/)
    - Secret API key
    - Public API Key
    - Debug mode (boolean)
    - Logs mode (boolean)

 * You may build a custom module to override this module with three hooks :
    - hook_openagenda_connector_create()
    - hook_openagenda_connector_update()
    - hook_openagenda_connector_delete()

 The goal is to transform a node object into an array of structured data

 * You may configure 3 rules :
    1/ Add an event Node (after deleting content, after saving new content, after updating existing content)
    2/ Add a condition Node (content is of type) and choose the type of content
    3/ Add an action custom (Delete on OpenAgenda or Publish on OpenAgenda)


MAINTAINERS
-----------

Current maintainers:
 * Sarah Grimard <sgrimard@gaya.fr>
 * Emmanuel Pelegrin <epelegrin@gaya.fr>

This project has been sponsored by:
 * Paris Musées - http://parismusees.paris.fr/
 * GAYA - http://www.gaya.fr


API DOCUMENTATION
-------------
https://openagenda.zendesk.com/hc/fr/sections/201090781-Zone-API-documentation-en-anglais-

