<?php
/**
 * Implements hook_menu().
 */
function openagenda_connector_menu() {
  $items['admin/config/services/openagenda-connector'] = array(
      'title' => t('Manage Open Agenda'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openagenda_connector_settings'),
      'access callback' => 'user_access',
      'access arguments' => array('Administer OpenAgenda Connector'),
      'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_permission
 */
function openagenda_connector_permission() {
  return array(
      'Administer OpenAgenda Connector' => array(
          'title' => 'Administer OpenAgenda Connector',
      ),
  );
}

/**
 * Helper function generates admin settings page.
 */
function openagenda_connector_settings($form, &$form_state) {

  $form = array();

  // Open Agenda
  $form['open_agenda'] = array(
      '#type' => 'fieldset',
      '#title' => 'OpenAgenda',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
  );
  $form['open_agenda']['open_agenda_end_point'] = array(
      '#type' => 'textfield',
      '#title' => t('Base URL of the API'),
      '#size' => 150,
      '#maxlength' => 250,
      '#description' => t('Enter here the Open Agenda API base URL'),
      '#default_value' => variable_get('open_agenda_end_point'),
      '#access' => user_access('Administer OpenAgenda Connector')
  );
  $form['open_agenda']['open_agenda_base_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Base URL for public access'),
      '#size' => 150,
      '#maxlength' => 250,
      '#description' => t('Ex : http://openagenda.com/'),
      '#default_value' => variable_get('open_agenda_base_url'),
      '#access' => user_access('Administer OpenAgenda Connector')
  );

  // Account
  $form['account'] = array(
      '#type' => 'fieldset',
      '#title' => t('Account'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
  );
  $form['account']['account_secret_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Secret API key'),
      '#size' => 150,
      '#maxlength' => 250,
      '#default_value' => variable_get('account_secret_api_key'),
      '#access' => user_access('Administer OpenAgenda Connector')
  );
  $form['account']['account_public_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Public API Key'),
      '#size' => 150,
      '#maxlength' => 250,
      '#default_value' => variable_get('account_public_api_key'),
      '#access' => user_access('Administer OpenAgenda Connector')
  );

  // Options
  $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
  );
  $form['options']['options_debug_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug Mode'),
      "#description" => t('Print the API responses into the screen'),
      '#default_value' => variable_get('options_debug_mode'),
      '#access' => user_access('Administer OpenAgenda Connector')
  );
  $form['options']['options_logs_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Logs Mode'),
      "#description" => t('Save the call to the API into watchdog OpenAgenda catecory'),
      '#default_value' => variable_get('options_logs_mode'),
      '#access' => user_access('Administer OpenAgenda Connector')
  );

  return system_settings_form($form);
}


/**
 * Function to connect to the API
 * param string $api_key
 * return string $token
 * @see "Publishing on OpenAgenda - Authentication" part in https://openagenda.zendesk.com/hc/fr/articles/203155742-Publish-content-through-the-API
 */
function openagenda_connector_connect($api_key = '') {
  module_load_include('inc', 'openagenda_connector', 'inc/openagenda_connector.connect');
  return _openagenda_connector_connect($api_key);
}


/**
 * Implements hook_openagenda_connector_create()
 * Create an event on OpenAgenda
 * @param string $token
 * @param array $data strutured data
 * @return int $event_uid
 */
function openagenda_connector_create($token, $data) {
  module_load_include('inc', 'openagenda_connector', 'inc/openagenda_connector.publish');
  $map_data = module_invoke_all('openagenda_connector_create', $token, $data); //call to the hook
  return _openagenda_connector_create($token, $map_data);
}

/**
 * Implements hook_openagenda_connector_update()
 * Update en event on OpenAgenda
 * @param string $token
 * @param int $eventUid
 * @param array $data structured data
 */
function openagenda_connector_update($token, $eventUid, $data) {
  module_load_include('inc', 'openagenda_connector', 'inc/openagenda_connector.publish');
  $map_data = module_invoke_all('openagenda_connector_update', $token, $eventUid, $data); //call to the hook
  _openagenda_connector_update($token, $eventUid, $map_data);
}


/**
 * Implements hook_openagenda_connector_delete()
 * Delete an event on OpenAgenda
 * @param string $token
 * @param int $eventUid
 */
function openagenda_connector_delete($token, $eventUid) {
  module_load_include('inc', 'openagenda_connector', 'inc/openagenda_connector.delete');
  module_invoke_all('openagenda_connector_delete', $token, $eventUid); //call to the hook
  _openagenda_connector_delete($token, $eventUid);
}


/*
 * Function to associate an event to an agenda
 * @param int $agendaUid
 * @param int $eventUid
 * @param string $token
 * @param $category (optional)
 */
function openagenda_connector_associate_event_to_agenda($agendaUid, $eventUid, $token, $category=null) {
  module_load_include('inc', 'openagenda_connector', 'inc/openagenda_connector.publish');
  _openagenda_connector_associate_event_to_agenda($agendaUid, $eventUid, $token, $category);
}


/**
 * Events need to be associated to locations at creation
 * @param string $token
 * @param array $data
 * @return int $location_uid
 * @see "Creating a location" part in https://openagenda.zendesk.com/hc/fr/articles/203155742-Publish-content-through-the-API
 */
function openagenda_connector_create_location($token, $data) {

  $location_uid = '';
  $url_connect = variable_get_value('open_agenda_end_point') . '/locations';

  $ch = curl_init($url_connect);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
      'access_token' => $token,
      'nonce' => rand(),
      'data' => json_encode(array(
          'placename' => $data['placename'],
          'address' => $data['address'],
          'latitude' => $data['latitude'],
          'longitude' => $data['longitude']
      ))
  ));

  $received_content = curl_exec($ch);

  if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
    $response = json_decode($received_content, true);
    $location_uid = $response['uid'];

    //log mode
    openagenda_connector_log_mode('Appel à openagenda_connector_create_location ', WATCHDOG_NOTICE);
  }
  return $location_uid;
}


/*
 * Function to get the UID of an agenda
 * param string $agenda_name
 * return int $agenda uid
 * @see "Getting a agenda Uid" part in https://openagenda.zendesk.com/hc/fr/articles/203222571-Get-event-data-through-the-API
 */
function openagenda_connector_get_agenda_uid($agenda_name) {

  $agendaUid = 0;
  $key = variable_get_value('account_public_api_key');
  if ($key && $agenda_name) {

    $url_connect = variable_get_value('open_agenda_end_point');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, "$url_connect/agendas/uid/$agenda_name?key=$key");

    // this is a get request
    curl_setopt($ch, CURLOPT_POST, false);

    $received_content = curl_exec($ch);

    $response = json_decode($received_content, true);

    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
      throw new Exception('Could not retrieve the agenda uid' . ($response !== null?': ' . $response['error_description']:''));
    }
    $agendaUid = $response['data']['uid'];

    //log mode
    openagenda_connector_log_mode('Appel à openagenda_connector_get_agenda_uid', WATCHDOG_NOTICE);
  }
  return $agendaUid;
}



/*
 * if the debug mode is active, the function returns are shown on the screen
 * @param string $type (name of the function called)
 * @param array $response
 */
function openagenda_connector_debug_mode($type, $response){
  if (variable_get_value('options_debug_mode')) {
    drupal_set_message($type . '  <pre>' . print_r($response,true) . '</pre>' , 'status');
  }
}


/*
 * If the mode log is active, save the call to the functions into watchdog
 * @param string $message
 * @param constant notice (watchdog notice)
 */
function openagenda_connector_log_mode($message, $notice){
  if (variable_get_value('options_logs_mode')) {
    watchdog('OpenAgenda', $message, array(), $notice);
  }
}